terraform {
  backend "s3" {
    bucket  = "recipe-app-api-devops"
    key     = "recipe-app.tfstate"
    region  = "us-east-1"
    encrypt = true
    # dynamo table: optional but for creating locks, only one can execute at the same time
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~>2.54.0"
}

# Locals are ways that you can create dynamic variables inside Terraform.
locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Contact     = var.contact
    ManagedBy   = "Terraform"
  }
}