variable "prefix" {
  default = "raad" # short for recipe app api devops
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "shinto.philip007@gmail.com"
}