//data is for retrieving information from AWS
data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name = "name"
    values = [
    "amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
  owners = [
  "amazon"]
}

// creating things in AWS
resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"
  tags = merge(local.common_tags,          // tags allow you to add metadata for your resources.
    map("Name", "${local.prefix}-bastion") // instance name
  )
}
// tags can be simple if we just need the name.
// tags = {
//    Name = "${local.prefix}-bastion"
//  }

